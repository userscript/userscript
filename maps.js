(function() {
    document.addEventListener('DOMContentLoaded', () => {
        // Your previous code to add the Google button.
        const toolbar = document.querySelector('.zcm__item:last-child');
        if (!toolbar) return;

        const googleBtn = document.createElement('a');
        googleBtn.textContent = 'Google';
        googleBtn.href = '#';
        googleBtn.className = 'zcm__link';
        googleBtn.onclick = () => {
            const query = document.getElementById('search_form_input').value;
            window.location.href = 'https://www.google.com/search?q=' + encodeURIComponent(query);
        };

        const googleItem = document.createElement('li');
        googleItem.className = 'zcm__item';
        googleItem.appendChild(googleBtn);

        toolbar.parentElement.insertBefore(googleItem, toolbar.nextSibling);

        // Code to intercept Google Maps direction links and redirect to Apple Maps.
        document.addEventListener('click', (e) => {
            const target = e.target;
            
            if (target.tagName.toLowerCase() === 'a' && target.href.includes('google.com/maps/dir')) {
                e.preventDefault();

                // Extract the destination from the Google Maps URL.
                const destinationParts = target.href.split('/').filter(part => part && !part.includes('google.com'));
                if (destinationParts.length > 1) {
                    const destination = destinationParts[1]; // Assumes destination is the second segment of the URL.

                    // Construct the Apple Maps URL and redirect.
                    const appleMapsUrl = 'https://maps.apple.com/?daddr=' + encodeURIComponent(destination);
                    window.location.href = appleMapsUrl;
                }
            }
        });
    });
})();